# Description #

C2P3 is a header only library implementing [KMeans-clustering](https://en.wikipedia.org/wiki/K-means_clustering)

C2P3 is short for Christians C++ Projects and is my attempt to implement a C++ library for educational purposes

### Requirements ###

To use :

* ** C++14 ** 

Additional requirements to build tests : 

* ** CMake: ** Version 3.5.1

### Testing ###

To compile and run tests, execute following commands in root : 

```
mkdir build && cd build && cmake .. && make && ctest && cd ..
```

If everything is good, you should see the following at the end : 

```
100% tests passed, 0 tests failed out of 5 
```

### Example ###

This examples is a complete program that you can build and run :

```C++
#include "c2p3.hpp"
#include <vector>
#include <tuple>

int main(){
    std::vector<c2p3::Point<float>> data {  { 1.1 , 1.3 , 0.4}
                                         ,  {-0.1 , 0.4 , 2.3}
                                         ,  {13.2 , 5.3 , 5.9} };
    auto numberOfClusters = 1;
    std::tuple<decltype(data),std::vector<std::size_t>> 
		result = c2p3::kmeans(data,numberOfClusters);

    std::vector<c2p3::Point<float>> centroids = std::get<0>(result);
    
	// Alternatively use a helper function to get correct field in tuple
	std::vector<std::size_t> assignments = c2p3::get<c2p3::kmeans_fields::assignments>(result);

    return 0;
}
```

### Documentation ###

Documentation can be generated using Doxygen. Execute `doxygen Doxyfile` in terminal at root then open file `html/index.htlm` in browser

### To do's ###

1. Revisit constexpr and noexcept tags on Point member functions. Not certain of correctness
2. Interface for KMeans function is clumsy. Should be revisited
3. More than one centroid can be initialized to the same point
4. Last test in Kmeans tests occasionally fails due to randomness in centroid initialization. Related to todo 3
5. Perform benchmark testing to check performance
