#include "catch.hpp"
#include "c2p3/kmeans.hpp"
#include <vector>


using namespace c2p3;

TEST_CASE("Empty data should return assignments","")
{
	std::vector<Point<float>> data;
	std::vector<Point<float>> centroid;
	std::vector<std::size_t> assignment; 
	
	detail::assign(data,centroid,assignment);
	REQUIRE( assignment.empty() );
}


TEST_CASE("Single data should return single assignment with value zero ","")
{
	std::vector<Point<float>> data {{0,0}};
	std::vector<Point<float>> centroids {{0,0}};
	
	// Init with a wrong assignment, i.e. something different from zero
	std::vector<std::size_t> assignments {data.size() + 1}; 
	
	detail::assign(data,centroids,assignments);
	
	std::vector<std::size_t> expected_assignments {0};
	REQUIRE( assignments == expected_assignments );
}

TEST_CASE("Assignment of a data point should be the index of closest centroid","")
{
	std::vector<Point<short unsigned int>> data {{0,0},{2,2},{10,10},{12,12}};
	std::vector<Point<short unsigned int>> centroids {{0,0},{11,11}};

	//Init with wrong assignments
	std::vector<std::size_t> assignments {0,0,0,0};

	detail::assign(data,centroids,assignments);
	std::vector<std::size_t> expected_assignments {0,0,1,1};

	REQUIRE( assignments == expected_assignments );
}

TEST_CASE("Centroids not closest to any data points should not be assigned","")
{
	std::vector<Point<double>> data {{0,0,0},{1,-1,0},{-2,2,2}};
	std::vector<Point<double>> centroids {{10,10,10},{0,0,0}};

	// Init with wrong assignments
	std::vector<std::size_t> assignments {data.size()+1,data.size()+1,data.size()+1};

	detail::assign(data,centroids,assignments);
	std::vector<std::size_t> expected_assignments {1,1,1};

	REQUIRE( assignments == expected_assignments );
}

