#include "catch.hpp"
#include "c2p3/kmeans.hpp"
#include <vector>

using namespace c2p3;

TEST_CASE("Empty data should return empty centroids" , "" )
{
	std::vector<Point<int>> data;
	std::vector<Point<int>> centroids;
	std::vector<std::size_t> assignments;
	
	//Run update on everything empty
	detail::update(data,centroids,assignments);
	
	REQUIRE( centroids.empty() );
}

TEST_CASE("Single data should return centroid equal to that data","")
{
	// 2 Dim
	std::vector<Point<double>> data2 {{1,-1}};
	std::vector<Point<double>> centroids2 {{0,0}};
	
	// Init with correct assignments
	std::vector<std::size_t> assignments2 {0};

	detail::update(data2,centroids2,assignments2);
	// Expected centroids is same as data
	std::vector<Point<double>> expected_centroids2 (data2);

	REQUIRE( centroids2 == expected_centroids2 ); 

	// 3 Dim
	std::vector<Point<double>> data3 {{1,3,-10.2}};
	std::vector<Point<double>> centroids3 {{0,0,0}};
	
	// Init with correct assignments
	std::vector<std::size_t> assignments3 {0};

	detail::update(data3,centroids3,assignments3);
	// Expected centroids is same as data
	std::vector<Point<double>> expected_centroids3 (data3);

	REQUIRE( centroids3 == expected_centroids3 ); 
}

TEST_CASE("Single centroid should be the average of its assigned data points","")
{
	// 3 Dim data is a cube
	std::vector<Point<short int>> data {{2,0,0},{0,2,0},{0,0,2},{2,2,2}};
	// Init centroids with incorrect values
	std::vector<Point<short int>> centroids {{0,0,0}};
	// Init with correct assignment
	std::vector<std::size_t> assignments {0,0,0,0};

	detail::update(data,centroids,assignments);

	std::vector<Point<short int>> expected_centroids {{1,1,1}};
	REQUIRE( centroids == expected_centroids );
}

TEST_CASE(" Centroids should be the average of its assigned data points","")
{
	// 2 Dim 
	std::vector<Point<float>> data {{0,0},{8,8},{0.5,0.5},{8.5,8.5},{1,1},{9,9}};
	// Init centroids with incorrect values
	std::vector<Point<float>> centroids {{1,-1},{4,0}};
	// Init with correct assignments
	std::vector<std::size_t> assignments {0,1,0,1,0,1};

	detail::update(data,centroids,assignments);
	
	std::vector<Point<float>> expected_centroids {{0.5,0.5},{8.5,8.5}};
	REQUIRE( centroids == expected_centroids );
}

TEST_CASE("Centroids with no assignments should not change","")
{
	std::vector<Point<double>> data {{0,0},{8,8},{0.5,0.5},{8.5,8.5},{1,1},{9,9}};
	// Init first centroid far away of data and second closer
	std::vector<Point<double>> centroids {{-100,-100},{1,1}};
	// Assign all datapoints to second centroid
	std::vector<std::size_t> assignments {1,1,1,1,1,1};
	
	detail::update(data,centroids,assignments);

	std::vector<Point<double>> expected_centroids {{-100,-100},{4.5,4.5}};

	REQUIRE( centroids == expected_centroids );
}
