#include "catch.hpp"
#include "c2p3/detail/kmeans.hpp"
#include <vector>

using namespace c2p3;

TEST_CASE( "Empty data should return empty centroids regardless of cluster numbers","" )
{
	std::vector<Point<float>> data;
	std::vector<Point<float>> centroids = detail::get_initialized_centroids(data,2);
	
	REQUIRE( centroids.empty() );
}

TEST_CASE("Number of centroids should be equal to number of clusters is data is not empty","")
{
	std::vector<Point<float>> data {{1,1},{0.2,3.2},{0.3234,101.212}};

	std::vector<Point<float>> centroids = detail::get_initialized_centroids(data,3);
	
	REQUIRE( centroids.size() == 3 );
}
