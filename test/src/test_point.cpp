#include "catch.hpp"

#include <iostream>

#include <forward_list>
#include <initializer_list>
#include <limits> // numeric_limits
#include <list>
#include <set>
#include <vector>

#include <typeinfo>

#include "c2p3/point.hpp"

template<template<typename> class F>
void template_test_runner() {
	INFO("(If type name is giberish, try pipeing test output to \"c++filt -t\"");
	INFO("Example : ./test | c++filt -t)");
	
	F<short unsigned int>::run();
	F<int>::run();
	F<long long int>::run();
	F<unsigned short int>::run();
	F<unsigned long long int>::run();
	F<float>::run();
	F<long double>::run();		
};

using namespace c2p3;

template<typename T>
struct template_test_1 {
	static void run(){
		INFO("Test type : " + std::string(typeid(T()).name()));
	
		Point<T> p;
		CHECK( p.dim() == 0 );

		auto v = p.getContainer();
		CHECK( v.empty() );
		auto firstVal = 2;
		auto lastVal = 9;
		Point<T> p2 {firstVal,0,0,0,lastVal};
			
		CHECK( *(p2.begin()) == firstVal );
		CHECK( *(p2.cbegin()) == firstVal );
			
		// TODO : Risky test statement ?
		CHECK( *(--p2.end()) == lastVal );

		CHECK( p2[0] == firstVal );
		CHECK( p2[p2.dim() - 1] == lastVal );
	}
};

TEST_CASE("Test default constructor and from intializer lists","")
{	
	template_test_runner<template_test_1>();
}

TEST_CASE ( "Comparison operators tests","" ) 
{
	Point<int> p1 {1,2,3,4};
	Point<double> p2 {1,2,3,4.0};
	Point<float> p3 {-1.5,4,3-3};
	Point<int> p4 {1,2,3,4,5};

	// Check equality
	CHECK( p1 == p1 );
	CHECK( p1 == p2 );
	CHECK( p2 == p1 );
	
	// Check inequality for same dims
	CHECK( p1 != p3 );
	CHECK( p2 != p3 );

	// Check inequality for diff dims
	CHECK( p1 != p4 );
	CHECK( p2 != p4 );
}

template<typename T>
struct template_test_2 {
	static void run(){
		INFO("Test type : " + std::string(typeid(T()).name()));
		
		std::initializer_list<T> il {1,4};
		std::forward_list<T> fl (il);
		std::list<T> l(il);
		std::set<T> s (il);

		Point<T> p_il (il);
		CHECK( p_il.dim() == il.size() );
		CHECK( std::equal(p_il.begin() , p_il.end(), il.begin() , il.end() ) );
	
		Point<T> p_fl (fl);
		CHECK( p_fl.dim() == std::distance(fl.begin(),fl.end()) );
		CHECK( std::equal( p_fl.begin(), p_fl.end() , fl.begin() , fl.end() ) );
	
		Point<T> p_l (l);	
		CHECK( p_l.dim() == l.size() );
		CHECK( std::equal( p_l.begin() , p_l.end() , l.begin() , l.end() ) );

		Point<T> p_s (s);	
		CHECK ( p_l.dim() == s.size() );
		CHECK( std::equal( p_s.begin(), p_s.end(), s.begin() , s.end() ) );
	}
};

TEST_CASE( "Constructor from different container types and value types","")
{
	template_test_runner<template_test_2>();
}

TEST_CASE( "Constructor from container type copy","")
{
	Point<double>::container_type v {3,4};
	Point<double> p_v (v);	
	CHECK( p_v.dim() == v.size() );
	CHECK( std::equal( p_v.begin(), p_v.end(), v.begin() , v.end() ) );
}

TEST_CASE( "Constructor from container type move","")
{
	Point<float>::container_type v {0,10};
	Point<float>::container_type v_copy (v);
	
	Point<float> p_v (std::move(v));

	CHECK( p_v.dim() == v_copy.size() );
	CHECK( std::equal(v_copy.begin(), v_copy.end(), p_v.begin() , p_v.end() ) );
}

TEST_CASE( "Copy assignment tests ","")
{
	Point<int> p1 {1,-2,3};
	Point<float> p2 {4,3-2,5};
	
	p2 = p1;
	CHECK( p1 == p2 );
	CHECK( typeid(p2[0]) ==  typeid(float) );	
}

TEST_CASE( "Addition tests" , "")
{
	// TODO : Test also for assignment to self
	Point<short int> p1 {-2,5,1};
	Point<int> p2 {3,-8,7,9};

	Point<float> p3 (p2 + p1);
	p2 += p1;
	
	Point<int> p2_expected {1,-3,8,9};
	Point<int> p3_expected {1,-3,8,9};
	CHECK( p2 == p2_expected );
	CHECK( p3 == p3_expected );
}

TEST_CASE( "Subtraction test","" )
{
	// TODO : Test also for assignment to self
	Point<std::string> ps;
	Point<float> p1 {2,-1,1};
	Point<double> p2 {9.25,-1,1,5};

	Point<long double> p3 (p2-p1);
	p2 -= p1;

	Point<double> p_expected {7.25,0,0,5};

	CHECK( p2 == p_expected );
	CHECK( p3 == p_expected );

	Point<double> p4 (-p_expected);
	Point<float> p4_expected {-7.25,0,0,-5};
	CHECK( p4 == p4_expected );
}

TEST_CASE ( "Test multiplication by scalar","" )
{
	Point<float> p1 {0,-1,3};
	p1 *= 2.0;
	Point<float> p1_expected {0,-2,6};
	CHECK( p1 == p1_expected );
	
	Point<double> p2;
	p2 = p1 * 0.25;
	Point<float> p2_expected {0,-0.5,1.5};
	CHECK( p2 == p2_expected );
}

TEST_CASE( "Test division by scalar","" ) 
{
	Point<float> p1 {0,-1,3};
	p1 /= 4.0;
	Point<float> p1_expected {0,-0.25,0.75};
	CHECK( p1 == p1_expected );
	
	Point<double> p2;
	p2 = p1_expected / 0.5;
	Point<float> p2_expected {0,-0.5,1.5};
	CHECK( p2 == p2_expected );
}

TEST_CASE ( "Test square","" ) 
{	
	Point<float> p1 {1,-1,1};
	CHECK( p1.squared() == 3 );
	
	Point<float> p2 {0,-0.25,0.75};
	CHECK( p2.squared() == 0.625 );
}
