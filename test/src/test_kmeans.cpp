#include "catch.hpp"
#include "c2p3/kmeans.hpp"
#include <vector>


using namespace c2p3;

TEST_CASE("Empty data should return empty centroids and empty assignments","")
{
	std::vector<Point<float>> data;
	auto tuple = kmeans(data,2,10);

	auto centroids = std::get<0>(tuple);
	auto assignments = std::get<1>(tuple);

	REQUIRE( centroids.empty() );
	REQUIRE( assignments.empty() );
}

TEST_CASE("Single data should return centroid equal to data and single assignment equal to 0","")
{
	std::vector<Point<float>> data {{1,1}};
	auto tuple = kmeans(data,1,10);

	auto centroids = std::get<0>(tuple);
	auto assignments = std::get<1>(tuple);

	std::vector<Point<float>> expected_centroids (data);
	std::vector<std::size_t> expected_assignments {0};
	
	REQUIRE( centroids == expected_centroids );
	REQUIRE( assignments == expected_assignments );
	
}

TEST_CASE("Single cluster should return centroid equal to data mean","")
{
	std::vector<Point<double>> data {{0,1,-1},{-2,4.5,3},{0,2.1,9},{1.4,9,-0.5}};
	auto tuple = kmeans(data,1,10);
	
	auto centroids = std::get<0>(tuple);
	auto assignments = std::get<1>(tuple);
	
	std::vector<Point<double>> expected_centroids {{-0.15,4.15,2.625}};
	std::vector<std::size_t> expected_assignments (data.size() , 0 );
	
	REQUIRE( centroids ==  expected_centroids );
	REQUIRE( assignments ==  expected_assignments );
}

// This test fails at times due to randomness in centroid initialization. Commented for now
/*
TEST_CASE( Multiple_Data_Multiple_Centroids )
{
	std::vector<Point<double>> data {{0,1},{-2,4.5},{0,2.1},{1.4,9},
									 {20.1,20.3},{22.9,21.2}};
	auto tuple = kmeans(data,2,10);
	
	auto centroids = std::get<0>(tuple);
	auto assignments = std::get<1>(tuple);
	
	std::vector<Point<double>> expected_centroids {{-0.15,4.15},{21.5, 20.75}};
	std::vector<std::size_t> expected_assignments {0,0,0,0,1,1};
	
	REQUIRE ( centroids == expected_centroids );
	REQUIRE ( assignments == expected_assignments );
	
}
*/
