include_directories(${C2P3_SOURCE_DIR}/include include)

add_executable(Test_Kmeans_Centroid_Initialization src/test_main.cpp src/test_kmeans_centroid_initialization.cpp)
add_executable(Test_Kmeans_Assignment src/test_main.cpp src/test_kmeans_assignment.cpp)
add_executable(Test_Kmeans_Update src/test_main.cpp src/test_kmeans_update.cpp)
add_executable(Test_Kmeans src/test_main.cpp src/test_kmeans.cpp)
add_executable(Test_Point src/test_main.cpp src/test_point.cpp)

add_test(NAME Test_Kmeans_Centroid_Initialization COMMAND Test_Kmeans_Centroid_Initialization)
add_test(NAME Test_Kmeans_Assignment COMMAND Test_Kmeans_Assignment)
add_test(NAME Test_Kmeans_Update COMMAND Test_Kmeans_Update)
add_test(NAME Test_Kmeans COMMAND Test_Kmeans)
add_test(NAME Test_Point COMMAND Test_Point)
