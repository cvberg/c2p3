/// @file 
/// @author Christian Berg 

#ifndef C2P3_KMEANS_HPP
#define C2P3_KMEANS_HPP

#include "c2p3/point.hpp" 
#include <tuple>


namespace c2p3 {

/// @brief Helper enum for keeping track of the field correpondance in the return tuple of kmeans
enum class kmeans_fields { centroids , assignments };

/// @brief A wrapper function for std::get but using c2p3::kmeans_fields enum as indexer.
/// @param arg The resulting tuple of kmeans
template<kmeans_fields field,typename T>
constexpr auto get(T&& arg) noexcept;

template<typename T> 
/// @brief Performs KMeans clustering
/// @param data Data to cluster
/// @param k Number clusters data should be partitionned in
/// @param max_it Max number of update and assignment iterations before returning estimate
/// @return A tuple containing centroids and assignments
std::tuple<std::vector<Point<T>>,std::vector<std::size_t>>
kmeans(const std::vector<Point<T>>& data,const std::size_t k, const std::size_t max_iterations = 30);

} //c2p3

#include "c2p3/impl/kmeans.ipp"

#endif // C2P3_KMEANS_HPP
