/// @file 
/// @author Christian Berg 

#ifndef C2P3_DETAIL_KMEANS_HPP
#define C2P3_DETAIL_KMEANS_HPP

#include "c2p3/point.hpp"

#include <algorithm> // transform, for_each, fill
#include <iterator> // back_inserter, distance
#include <random> // rand
#include <tuple>


namespace c2p3 {
namespace detail {
/* -------------------------------*/
/// @brief Initializes centroids to random samples from data
/// @param data Data to cluster
/// @param k Number of centroids to generate ( == number of clusters to partition data in)
/// @return Initialized centroids
template<typename T>
std::vector<Point<T>> 
get_initialized_centroids(const std::vector<Point<T>>& data,std::size_t k)
{
	std::vector<Point<T>> centroids;
	if (data.empty()){
		return centroids;
	}

	centroids.reserve(k); // TODO : How to handle if more clusters than data?

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, data.size() -1 );

	auto getRandomData = [&data,&dis,&gen](const auto& dummy){return data.at(dis(gen));};
	std::transform(centroids.begin() , centroids.begin() + k , std::back_inserter(centroids), getRandomData );
	return centroids;
}	

/* -------------------------------*/
/// @brief KMeans assignment step : assign data to centroids
/// @param[in] data Data to cluster
/// @param[in] centroids Cluster centroids
/// @param[out] assignments Updated assignments
template<typename T,typename V>
void 
assign	(	const std::vector<Point<T>>& data
		,	const std::vector<Point<T>>& centroids
		,	std::vector<V>& assignments)
{
	auto getIndexOfClosestCentroid = [&centroids](const auto& point)
	{
		std::vector<T> distances;
		distances.reserve(centroids.size());
		auto getDistanceFromCentroid = [&point](const auto& centroid){return (centroid - point).squared();};
		std::transform(centroids.begin() , centroids.end() , std::back_inserter(distances) , getDistanceFromCentroid);
		// Return index of centroid closest to p
		return std::distance(distances.begin(),std::min_element(distances.begin(),distances.end()));
	};
	// For all data, transform assignments to index of centroid closest to data
	std::transform(data.cbegin() , data.cend() , assignments.begin() , getIndexOfClosestCentroid );
};

/* -------------------------------*/
/// @brief KMeans update step : Update centroids according to data assignments
/// @param data Data to cluster
/// @param centroids Update centroids
/// @param assignments Cluster assignment of each data
template<typename T,typename V>
void 
update	(	const std::vector<Point<T>>& data
		,	std::vector<Point<T>>& centroids
		,	const std::vector<V>& assignments)
{
	auto old (centroids);
	std::vector<std::size_t> assignment_counts (centroids.size(),0);

	auto accumulateDataAndCountAssignment = [&centroids,&assignment_counts](const auto& data_point, const auto& assignment)
	{
		centroids[assignment] += data_point;
		++assignment_counts.at(assignment);
	};

	// For every data point and its assignment, add the data to its centroids and increment assignment counter
	// This is an adaptation of algorithms for_each, but for multiple iterators (imitation of boost iterators)
	for(auto iterators = std::make_tuple(data.cbegin() , assignments.cbegin());
		std::get<0>(iterators)!=data.cend() && std::get<1>(iterators)!= assignments.cend();
		std::get<0>(iterators)++ , std::get<1>(iterators)++)
	{
		accumulateDataAndCountAssignment(*std::get<0>(iterators),*std::get<1>(iterators));
	}
	
	auto getCentroidUpdate = [](const auto& updated_centroid, const auto& old_centroid , const auto& assignment_count)
	{
		// If data was assigned to centroid, return mean of data. Else return the centroids' old value. 
		return assignment_count > 0 ? (updated_centroid - old_centroid)/ assignment_count : old_centroid;
	};

	// For every centroid, update it with the mean of the assigned data points. If nothing was assigned to centroids, keep old value
	// This is an adaptation of algorithms transform, but for multiple iterators (imitation of boost iterators)
	for(auto iterators = std::make_tuple(centroids.begin() , old.cbegin() , assignment_counts.cbegin());
		std::get<0>(iterators)!=centroids.end();
		std::get<0>(iterators)++,std::get<1>(iterators)++,std::get<2>(iterators)++)
	{
		*std::get<0>(iterators) = getCentroidUpdate(*std::get<0>(iterators),*std::get<1>(iterators),*std::get<2>(iterators));
	}
}

} // detail
} //c2p3


#endif // C2P3_DETAIL_KMEANS_HPP
