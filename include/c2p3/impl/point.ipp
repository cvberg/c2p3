/// @file
/// @author Christian Berg

#ifndef C2P3_IMPL_POINT_IPP
#define C2P3_IMPL_POINT_IPP

#include <numeric> // accumulate
#include <algorithm> // transform , equal
#include <functional> // minus, plus, negate
#include <iostream>

namespace c2p3 {

// Default ctor
template<typename T>
Point<T>::Point()
noexcept 
	: m_dim() 
	, m_container()
{}

// Ctor from intializer_list of same type
template<typename T>
Point<T>::Point(std::initializer_list<T> l)
	: m_dim(l.size())
	, m_container(l)
{}

// Ctor from intializer_list of different type
template<typename T>
template<typename U>
Point<T>::Point(std::initializer_list<U> l)
	: m_dim(l.size())
	, m_container(l.begin(),l.end())
{}

// Ctor from other container
template<typename T>
template<class C>
Point<T>::Point(const C& c)
	: m_container(c.cbegin(),c.cend())
{
	m_dim = m_container.size();	
}

// Ctor from container_type copy
template<typename T>
Point<T>::Point(const Point::container_type& v)
noexcept
	: m_dim(v.size())
	, m_container(v)
{}
	
// Ctor from container_type move
template<typename T>
Point<T>::Point(Point::container_type&& v)
noexcept
	: m_dim(v.size())
	, m_container(std::move(v))
{}

// Addition assignment
template<typename T>
Point<T>&
Point<T>::operator+=(const Point& rhs)
{
	std::transform (begin(), begin() + rhs.dim() , rhs.cbegin(), begin(), std::plus<T>());
	return *this;
}

// Subtraction assignment 
template<typename T>
Point<T>&
Point<T>::operator-=(const Point& rhs)
{
	std::transform( begin(), begin() + rhs.dim() , rhs.cbegin(), begin(), std::minus<T>());
	return *this;
}

// Negation
template<typename T>
const Point<T>
Point<T>::operator-()
const
{
	auto tmp (*this);
	std::transform(tmp.begin() , tmp.end() , tmp.begin() , std::negate<T>() );
	return tmp;
}

// Assignment multiplication with a scalar
template<typename T>
template<typename U>
Point<T>&
Point<T>::operator*=(U scalar)
{
	std::transform(begin() , end() , begin() , [scalar](T point_elem){return point_elem*scalar;});
	return *this;
}

// Division assignment by scalar
template<typename T>
template<typename U>
Point<T>& 
Point<T>::operator/=(U scalar)
{
	// TODO : Handle division by zero ?
	std::transform(begin() , end() , begin() , [scalar](T point_elem){return point_elem/scalar;});
	return *this;
}

// Equal operator
template<typename T>
bool 
Point<T>::operator==(const Point& rhs)
const
{
	return std::equal(cbegin() , cend() , rhs.cbegin() , rhs.cend() );
}

// Not equal operator
template<typename T>
bool
Point<T>::operator!=(const Point& rhs)
const
{
	return !(*this == rhs);
}

// Euclidian Norm squared (Sum of elements squared)
template<typename T>
double 
Point<T>::squared()
const
{
	return std::accumulate(cbegin(),cend(),T(),[](const T& cumul, const T& elem){return cumul + elem*elem;});
}

// Getter for begin of internal container
template<typename T>
typename Point<T>::container_type::iterator
Point<T>::begin()
noexcept
{
	return m_container.begin();
}

// Getter for const begin of internal container
template<typename T>
typename Point<T>::container_type::const_iterator
Point<T>::cbegin()
const noexcept
{
	return m_container.cbegin();
}

// Getter for end of internal container
template<typename T>
typename Point<T>::container_type::iterator
Point<T>::end()
noexcept
{
	return m_container.end();
}

// Getter for const end of internal container
template<typename T>
typename Point<T>::container_type::const_iterator
Point<T>::cend()
const noexcept
{
	return m_container.cend();
}

// Bracket operator 
template<typename T>
T& 
Point<T>::operator[](std::size_t index)
{
	// TODO : brackets or at?
	return m_container[index];
}

// Getter for internal container
template<typename T>
typename Point<T>::container_type&
Point<T>::getContainer()
noexcept 
{
	return m_container;
}

// Getter for dimension
template<typename T>
typename Point<T>::dimension_type
Point<T>::dim() 
const noexcept 
{	
	return m_dim;
}

// Addition
template<typename T,typename U>
const Point<T>
operator+(const Point<T>& lhs,const Point<U>& rhs)
{
	return Point<T>(lhs)+=rhs;
}

// Subtraction 
template<typename T,typename U>
const Point<T>
operator-(const Point<T>& lhs,const Point<U>& rhs)
{
	return Point<T>(lhs)-=rhs;
}

// Multiplication with a scalar
template<typename T,typename U>
const Point<T>
operator*(const Point<T>& lhs,U scalar)
{
	return Point<T>(lhs) *= scalar;
}

// Division by scalar
template<typename T,typename U>
const Point<T>
operator/(const Point<T>& lhs,U scalar)
{
	return Point<T>(lhs) /= scalar;
}

} //c2p3
#endif //C2P3_IMPL_POINT_IPP
