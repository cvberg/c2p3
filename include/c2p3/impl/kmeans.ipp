/// @file
/// @author Christian Berg

#ifndef C2P3_IMPL_KMEANS_IPP
#define C2P3_IMPL_KMEANS_IPP

#include "c2p3/point.hpp"
#include "c2p3/detail/kmeans.hpp"

#include <type_traits>

namespace c2p3 {

/*--------------------------------------------*/
// Forward declaration. Definition in interface header
enum class kmeans_fields;

/*--------------------------------------------*/
template<kmeans_fields field,typename T>
constexpr auto
get(T&& arg)
noexcept
{
	return std::get<static_cast<std::underlying_type_t<kmeans_fields>>(field)>(std::forward<T>(arg));
}

/*--------------------------------------------*/
template<typename T>
std::tuple<std::vector<Point<T>>,std::vector<std::size_t>> 
kmeans(const std::vector<Point<T>>& data, const std::size_t k, const std::size_t max_iterations)
{	
	// Init assignment with wrong values
	std::vector<std::size_t> assignments (data.size(),k+1);
	auto centroids = detail::get_initialized_centroids(data,k);

	std::size_t count {0};
	while(count < max_iterations){
		auto old (centroids);
		detail::assign(data,centroids,assignments);
		detail::update(data,centroids,assignments);
		std::equal(centroids.begin() , centroids.end() , old.begin() , old.end() ) ? count = max_iterations : count ++;
	}
	return std::make_tuple(centroids,assignments);
};

} //c2p3
#endif //C2P3_IMPL_KMEANS_IPP
