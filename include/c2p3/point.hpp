/// @file
/// @author Christian Berg 

#ifndef C2P3_POINT_HPP
#define C2P3_POINT_HPP

#include <type_traits>
#include <vector>
#include <initializer_list>

namespace c2p3 {
/* -------------------------------*/

template<typename T>
class Point final {
public:
	/// @brief Type of data contained in container m_c
	using value_type = T;

	/// @brief Type of dimension attribute
	using dimension_type = std::size_t;

	/// @brief Type of container
	using container_type = std::vector<T>;

	/// @brief Default constructor
	Point() noexcept;
		
	/// @brief Constructor from an initializer_list of same value_type
	Point(std::initializer_list<T> l);

	/// @brief Constructor from an initializer_list of different value_type
	template<typename U>
	Point(std::initializer_list<U> l);

	/// @brief Constructor from a different container 
	template<class C>
	Point(const C& c);

	/// @brief Constructor from a container_type copy  
	Point(const container_type& v) noexcept;

	/// @brief Constructor from a container_type move
	Point(container_type&& v) noexcept;

	/// @brief Default copy construct
	Point(const Point& p) = default;

	/// @brief Default move construct
	Point(Point&& p) = default;

	/// @brief Default copy assignment
	Point& operator=(const Point& p) = default;
	
	/// @brief Default move assignment
	Point& operator=(Point&& p) = default;

	// Dtor 
	~Point() = default;	

	/// @brief Addition assignment
	Point& operator+=(const Point& rhs);

	/// @brief Subtraction assignment
	Point& operator-=(const Point& rhs);

	/// @brief Negation 
	const Point operator-() const;

	/// @brief Assignment multiplication with a scalar
	template<typename U> 
	Point& operator*=(U scalar);

	/// @brief Division assignment by scalar
	template<typename U>
	Point& operator/=(U scalar);

	/// @brief Equal operator
	bool operator==(const Point& rhs) const;

	/// @brief Not equal operator
	bool operator!=(const Point& rhs) const;

	/// @brief Get Euclidian Norm squared (Sum of elements squared)
	double squared() const;

	/// @brief Getter to iterator to first point component
	typename container_type::iterator begin() noexcept; 
	
	/// @brief Getter to const iterator to first point component
	 typename container_type::const_iterator cbegin() const noexcept; 
	
	/// @brief Getter to iterator to past-the-last point component 
	typename container_type::iterator end() noexcept;

	/// @brief Getter to const iterator to past-the-last point component
	typename container_type::const_iterator cend() const noexcept;

	/// @brief Getter point component at index
	T& operator[](std::size_t index);

	// TODO : Remove this? Scott Meyer item 28 : 
	// "Avoid returninghandles to internal objects"
	/// @brief Getter to internal point handle
	container_type& getContainer() noexcept;  
	
	/// @brief Getter point dimension
	dimension_type dim() const noexcept;
private:
	dimension_type m_dim;
	container_type m_container;
};

/// @brief Addition
template<typename T,typename U>
const Point<T> operator+(const Point<T>& lhs,const Point<U>& rhs);

/// @brief Subtracion
template<typename T,typename U>
const Point<T> operator-(const Point<T>& lhs,const Point<U>& rhs);

/// @brief Multiplication with a scalar
template<typename T,typename U>
const Point<T> operator*(const Point<T>& lhs,U scalar);	

/// @brief Division by scalar
template<typename T,typename U>
const Point<T> operator/(const Point<T>& lhs,U scalar);

} //c2p3

#include "c2p3/impl/point.ipp"

#endif // C2P3_POINT_HPP
